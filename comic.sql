-- Written by Tyler Brandt
DROP DATABASE IF EXISTS comic;
CREATE DATABASE comic;

USE comic;

-- Conventions Table
DROP TABLE IF EXISTS conventions;
CREATE TABLE IF NOT EXISTS conventions (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO conventions (id, name) VALUES
(1, 'Convention 1'),
(2, 'Convention 2'),
(3, 'Convention 3'),
(4, 'Convention 4'),
(5, 'Convention 5'),
(6, 'Convention 6'),
(7, 'Convention 7'),
(8, 'Convention 8'),
(9, 'Convention 9'),
(10, 'Convention 10'),
(11, 'Convention 11'),
(12, 'Convention 12'),
(13, 'Convention 13');
-- End Conventions Table


-- Events Table
DROP TABLE IF EXISTS events;
CREATE TABLE IF NOT EXISTS events (
  eventid int(11) NOT NULL AUTO_INCREMENT,
  start time NOT NULL,
  end time NOT NULL,
  day date NOT NULL,
  brief varchar(500) NOT NULL,
  extended varchar(3000) NOT NULL,
  location varchar(300) NOT NULL,
  convention varchar(50) NOT NULL,
  category varchar(50) NOT NULL,
  featured boolean NOT NULL,
  PRIMARY KEY (eventid),
  KEY convention (convention)
);

INSERT INTO events (eventid, start, end, day, brief, extended, location, convention, category, featured) VALUES
(1, '00:00:00', '00:00:01', '2001-01-01', 'brief 1', 'extended 1', 'location 1', '1', 'games', TRUE),
(15, '00:00:00', '00:00:01', '2001-01-01', 'brief 1', 'extended 1', 'location 1', '1', 'games', TRUE),
(2, '01:00:00', '01:00:01', '2001-02-01', 'brief 2', 'extended 2', 'location 2', '2', 'games', TRUE),
(3, '02:00:00', '02:00:01', '2001-03-01', 'brief 3', 'extended 3', 'location 3', '3', 'games', TRUE),
(4, '03:00:00', '03:00:01', '2001-04-01', 'brief 4', 'extended 4', 'location 4', '4', 'games', TRUE),
(5, '04:00:00', '04:00:01', '2001-05-01', 'brief 5', 'extended 5', 'location 5', '5', 'games', TRUE),
(6, '05:00:00', '05:00:01', '2001-06-01', 'brief 6', 'extended 6', 'location 6', '6', 'games', TRUE),
(7, '06:00:00', '06:00:01', '2001-07-01', 'brief 7', 'extended 7', 'location 7', '7', 'games', FALSE),
(8, '07:00:00', '07:00:01', '2001-08-01', 'brief 8', 'extended 8', 'location 8', '8', 'movies', FALSE),
(9, '08:00:00', '08:00:01', '2001-09-01', 'brief 9', 'extended 9', 'location 9', '9', 'movies', FALSE),
(10, '09:00:00', '09:00:01', '2001-10-01', 'brief 10', 'extended 10', 'location 10', '10', 'movies', FALSE),
(11, '10:00:00', '10:00:01', '2001-11-01', 'brief 11', 'extended 11', 'location 11', '11', 'movies', FALSE),
(12, '11:00:00', '11:00:01', '2001-12-01', 'brief 12', 'extended 12', 'location 12', '12', 'movies', FALSE),
(13, '12:00:00', '12:00:01', '2001-12-02', 'brief 13', 'extended 13', 'location 13', '13', 'movies', FALSE),
(14, '12:00:02', '12:00:03', '2001-12-03', 'brief 14', 'extended 14', 'location 14', '13', 'movies', FALSE),
(21, '00:00:00', '00:00:01', '2001-01-01', 'brief 1', 'extended 1', 'location 1', '2', 'games', TRUE),
(215, '00:00:00', '00:00:01', '2001-01-01', 'brief 1', 'extended 1', 'location 1', '2', 'games', TRUE),
(22, '01:00:00', '01:00:01', '2001-02-01', 'brief 2', 'extended 2', 'location 2', '2', 'games', TRUE),
(23, '02:00:00', '02:00:01', '2001-03-01', 'brief 3', 'extended 3', 'location 3', '3', 'games', TRUE),
(24, '03:00:00', '03:00:01', '2001-04-01', 'brief 4', 'extended 4', 'location 4', '4', 'games', TRUE),
(25, '04:00:00', '04:00:01', '2001-05-01', 'brief 5', 'extended 5', 'location 5', '5', 'games', TRUE),
(26, '05:00:00', '05:00:01', '2001-06-01', 'brief 6', 'extended 6', 'location 6', '6', 'games', TRUE),
(27, '06:00:00', '06:00:01', '2001-07-01', 'brief 7', 'extended 7', 'location 7', '7', 'games', FALSE),
(28, '07:00:00', '07:00:01', '2001-08-01', 'brief 8', 'extended 8', 'location 8', '8', 'movies', FALSE),
(29, '08:00:00', '08:00:01', '2001-09-01', 'brief 9', 'extended 9', 'location 9', '9', 'movies', FALSE),
(210, '09:00:00', '09:00:01', '2001-10-01', 'brief 10', 'extended 10', 'location 10', '10', 'movies', FALSE),
(211, '10:00:00', '10:00:01', '2001-11-01', 'brief 11', 'extended 11', 'location 11', '11', 'movies', FALSE),
(212, '11:00:00', '11:00:01', '2001-12-01', 'brief 12', 'extended 12', 'location 12', '12', 'movies', FALSE),
(213, '12:00:00', '12:00:01', '2001-12-02', 'brief 13', 'extended 13', 'location 13', '13', 'movies', FALSE),
(214, '12:00:02', '12:00:03', '2001-12-03', 'brief 14', 'extended 14', 'location 14', '13', 'movies', FALSE);
-- End Events Table


-- Schedules table
DROP TABLE IF EXISTS schedules;
CREATE TABLE IF NOT EXISTS schedules (
  id int(11) NOT NULL AUTO_INCREMENT,
  user varchar(100) NOT NULL,
  event varchar(50) NOT NULL,
  note varchar(500),
  overlapping boolean,
  PRIMARY KEY (id),
  KEY event (event)
);

INSERT INTO schedules (id, user, event, note, overlapping) VALUES
(1, '1', '1', 'it was cool', 'false'),
(2, '1', '2', 'it was cool', 'false'),
(3, '1', '3', 'it was cool', 'false'),
(4, '1', '4', 'it was cool', 'false'),
(5, '1', '5', 'it was cool', 'false'),
(6, '1', '6', 'it was cool', 'false'),
(7, '1', '7', 'it was cool', '1'),
(8, '1', '8', 'it was fun', '1'),
(9, '1', '9', 'it was fun', '1'),
(10, '1', '10', 'it was fun', '1'),
(11, '1', '11', 'it was fun', '1'),
(12, '1', '12', 'it was fun', '1'),
(13, '1', '13', 'it was fun', '1');
-- End Schedules Table