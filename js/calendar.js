//Written by Tyler Brandt

$(document).ready(function() {
	$('#accordion').accordion({collapsible: true});
	$('.added').tooltip({position:{at:"center top", my:"center bottom"}});

	$('.expandButton').on("click", initializeExpandButton);
	$('.addButton').on("click", addEvent);
	$('.removeButton').on("click", removeEvent);
	$('td').on("click", filterConventions);

	$('.start').each(function(){
		$(this).html($(this).html().substring(0,5));
	});
	$('.stop').each(function(){
		$(this).html($(this).html().substring(0,5));
	});
	$('.day').each(function(){
		var date = $(this).html();
		var d = new Date(date);
		d = d.toLocaleString();
		d = d.substring(0, d.indexOf(','));
		$(this).html(d.toLocaleString());
	});
	$('#hideOverlap').on("click", function(){
		$(this).parent().hide();
	})
});

function filterConventions(){
	var convention = $(this).html();

	if(convention == "All Conventions"){
		$('.event').each(function(){
			$(this).show();
			$(this).parent().parent().show();
		});
		$('h3').each(function(){
			$(this).show();
		});
	}else{
		$('.event').each(function(){
			var con = $(this).children('.convention').html();
			if(convention != con){
				$(this).hide();
				var id = $(this).parent().parent().prop("id");
				$('h3[id=' + id + ']').hide();
				$(this).parent().parent().hide();
			}else{
				$(this).show();
				var id = $(this).parent().parent().prop("id");
				$('h3[id=' + id + ']').show();
				$(this).parent().parent().show();
			}
		});
	}
	$('.ui-accordion-content').each(function(){
		$(this).removeClass('.ui-accordion-content-active')
		$(this).prop("style", "border: 1px solid; display: none; height: 366px;");
	});
	$('.ui-accordion-header').prop("class", "ui-accordion-header ui-corner-top ui-accordion-header-collapsed ui-corner-all ui-state-default ui-accordion-icons");

	$('td').each(function(){
		$(this).prop("style", "")
	});
	$(this).prop("style", "background-color: #000000;	color: #FFFFFF;")
}

function addEvent(){
	var id = $(this).siblings('.eventId').val();
	var self = $(this);

	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function(){

		if(xmlhttp.readyState == xmlhttp.DONE){
			if(xmlhttp.response == "success"){
				self.parent().addClass("added");
				self.parent().prop("title", "This event has been added to your schedule");
				self.parent().tooltip({position:{at:"center top", my:"center bottom"}});
				$('#addNotification').css("visibility", "visible");
				$('#addNotification').animate({backgroundColor: "#FFFFFF"}, {complete: function() { $('#addNotification').css("visibility", "hidden"); $('#addNotification').attr("style", "");}, duration: 1000});
				self.hide();
				self.siblings('.removeButton').show();
			}else if(xmlhttp.response == "overlap"){
				self.parent().addClass("added");
				self.parent().prop("title", "This event has been added to your schedule");
				self.parent().tooltip({position:{at:"center top", my:"center bottom"}});
				$('#overlapNotification').css("visibility", "visible");
				$('#overlapNotification').animate({backgroundColor: "#FFFFFF"}, {complete: function() 
					{ 
						setTimeout(function(){
							$('#overlapNotification').hide();
							$('#overlapNotification').attr("style", "");
						}, 1500);
					}, duration: 1000});
				self.hide();
				self.siblings('.removeButton').show();
				self.siblings('.conflictLabel').show();
			}
		}
	}
	xmlhttp.open("POST", 'add.php', true);
	xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xmlhttp.send("id=" + id);
}

function removeEvent(){
	var id = $(this).siblings('.eventId').val();
	var self = $(this);

	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState == xmlhttp.DONE){
			if(xmlhttp.response == "success"){
				self.parent().removeClass("added");
				self.parent().prop("title", "");
				self.parent().tooltip('disable');
				$('#removeNotification').css("visibility", "visible");
				$('#removeNotification').animate({backgroundColor: "#FFFFFF"}, {complete: function() { $('#removeNotification').css("visibility", "hidden"); $('#removeNotification').attr("style", "");}, duration: 1000});
				self.hide();
				self.siblings('.addButton').show();
				self.siblings('.conflictLabel').hide();
			}else{
			
			}
		}
		
	}
	xmlhttp.open("POST", 'drop.php', true);
	xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xmlhttp.send("id=" + id);
}

function initializeExpandButton(){
	if($(this).prop("expanded") == "true"){
		$(this).parent().css("height", "80px");
		$(this).siblings('.long').css("visibility", "hidden");
		$(this).prop("expanded", "false");
	}else{
		$(this).parent().css("height", "150px");
		$(this).siblings('.long').css("visibility", "visible");
		$(this).prop("expanded", "true");
	}
}