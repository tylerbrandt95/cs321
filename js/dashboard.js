//Written by Tyler Brandt

$(function(){
	$('#accordion').accordion();
	$('.added').tooltip({position:{at:"center top", my:"center bottom"}});

	$('.expandButton').on("click", initializeExpandButton);
	$('.addButton').on("click", addEvent);
	$('.removeButton').on("click", removeEvent);

	$('.start').each(function(){
		$(this).html($(this).html().substring(0,5));
	});
	$('.stop').each(function(){
		$(this).html($(this).html().substring(0,5));
	});
	$('.day').each(function(){
		var date = $(this).html();
		var d = new Date(date);
		d = d.toLocaleString();
		d = d.substring(0, d.indexOf(','));
		$(this).html(d.toLocaleString());
	});

	$('#featuredHeader').on("click", function(){
		$("#featuredevents").toggle(500);
	});
	$('#scheduleHeader').on("click", function(){
		$("#scheduleEvents").toggle(500);
	});

});

function addEvent(){
	var id = $(this).siblings('.eventId').val();
	var self = $(this);

	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function(){

		if(xmlhttp.readyState == xmlhttp.DONE){
			if(xmlhttp.response == "success"){
				self.parent().addClass("added");
				self.parent().prop("title", "This event has been added to your schedule");
				self.parent().tooltip({position:{at:"center top", my:"center bottom"}});
				$('#addNotification').css("visibility", "visible");
				$('#addNotification').animate({backgroundColor: "#FFFFFF"}, {complete: function() { $('#addNotification').css("visibility", "hidden"); $('#addNotification').attr("style", "");}, duration: 1000});
				self.hide();
				self.siblings('.removeButton').show();
			}else if(xmlhttp.response == "overlap"){
				self.parent().addClass("added");
				self.parent().prop("title", "This event has been added to your schedule");
				self.parent().tooltip({position:{at:"center top", my:"center bottom"}});
				$('#overlapNotification').css("visibility", "visible");
				$('#overlapNotification').animate({backgroundColor: "#FFFFFF"}, {complete: function() 
					{ 
						setTimeout(function(){
							$('#overlapNotification').hide();
							$('#overlapNotification').attr("style", "");
						}, 1500);
					}, duration: 1000});
				self.hide();
				self.siblings('.removeButton').show();
				self.siblings('.conflictLabel').show();
			}
		}
	}
	xmlhttp.open("POST", 'add.php', true);
	xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xmlhttp.send("id=" + id);
}

function removeEvent(){
	var id = $(this).siblings('.eventId').val();
	var self = $(this);

	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState == xmlhttp.DONE){
			if(xmlhttp.response == "success"){
				self.parent().removeClass("added");
				self.parent().prop("title", "");
				self.parent().tooltip('disable');
				$('#removeNotification').css("visibility", "visible");
				$('#removeNotification').animate({backgroundColor: "#FFFFFF"}, {complete: function() { $('#removeNotification').css("visibility", "hidden"); $('#removeNotification').attr("style", "");}, duration: 1000});
				self.hide();
				self.siblings('.addButton').show();
				self.siblings('.conflictLabel').hide();

				if(self.parent().parent().prop("id") == "scheduleEvents"){
					self.parent().remove();
				}
			}else{
			
			}
		}
		
	}
	xmlhttp.open("POST", 'drop.php', true);
	xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xmlhttp.send("id=" + id);
}

function initializeExpandButton(){
	if($(this).prop("expanded") == "true"){
		$(this).parent().css("height", "80px");
		$(this).siblings('.long').css("visibility", "hidden");
		$(this).prop("expanded", "false");
	}else{
		$(this).parent().css("height", "150px");
		$(this).siblings('.long').css("visibility", "visible");
		$(this).prop("expanded", "true");
	}
}