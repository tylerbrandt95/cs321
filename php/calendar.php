<html>

<head>
	<link rel="stylesheet" href="../css/index.css">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="../css/calendar.css">
</head>
<body>

<div class = "header">
Calendar</div>


<div class = "sidebar">

<ul>

<a href = "Dashboard.php">
<li>
Dashboard
</li>
</a>

<a href = "Calendar.php">
<li>
Calendar
</li>
</a>

<a href = "Schedule.php">
<li>
Schedule
</li>
</a>

<a href = "Search.php">
<li>
Search
</li>
</a>

</ul>

</div>

<div id="wonder">
<img src="../images/wonder.jpg">
</div>
<div id="thor">
<img src="../images/thor.jpg">
</div>
<div id="flash">
<img src="../images/flash.png">
</div>
<div id="hulk">
<img src="../images/hulk.jpg">
</div>

<div id="iron">
<img src="../images/iron.jpg">
</div>
<div id="superman">
<img src="../images/superman.png">
</div>

<!-- Below code written by Tyler Brandt -->
<div id="addNotification">Event Added!</div>
<div id="removeNotification">Event Removed :(</div>
<div id="overlapNotification">
    Event Added! <br>This event has caused a time conflict in your schedule
</div>

<div id="conventionSelect">
    <table id="conventionTable">
        <tr>
            <td>All Conventions</td>
            <td>Convention 1</td>
            <td>Convention 2</td>
            <td>Convention 3</td>
            <td>Convention 4</td>
            <td>Convention 5</td>
            <td>Convention 6</td>
        </tr>
        <tr>
            <td>Convention 7</td>
            <td>Convention 8</td>
            <td>Convention 9</td>
            <td>Convention 10</td>
            <td>Convention 11</td>
            <td>Convention 12</td>
            <td>Convention 13</td>
        </tr>
    </table>
</div>
<div id="wrapper">
<div id="accordion">
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "comic";

$conn = new mysqli($servername, $username, $password, $dbname);

if($conn->connect_error){
	die("connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM events INNER JOIN conventions on events.convention = conventions.id
							 LEFT JOIN schedules on events.eventid = schedules.event
							 ORDER BY events.day ASC;";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $lastDay = null;
    $days = 1;
    while($row = $result->fetch_assoc()) {

    	$day = $row['day'];

    	if($lastDay != null){
    		if($day != $lastDay){
                $days++;
    			echo "
    				</ul>
    			</div>
    			<h3 id='".$days."'>" . $day . "</h3>
    			<div id='".$days."' style='border:solid 1px;'>
    				<ul class='eventList' >";
    		}
    	}else{
    		echo "
    		<h3 id='".$days."'>" . $day . "</h3>
    		<div id='".$days."' style='border:solid 1px;'>
    			<ul class='eventList' >";
    	}
    	$lastDay = $day;
    	
        echo "
        <li class='event " . ($row['event'] != null ? "added" : "") . "' title='This event has been added to your schedule'>
        	<input  class='eventId' type='text' value='" . $row['eventid'] . "' hidden>
	        <label  class='timeLabel'         > Time/Date</label>
			<label  class='descriptionLabel'  > Description</label>
			<label  class='conventionLabel'   > Convention</label>
			<label  class='locationLabel'     > Location</label>
			<label  class='categoryLabel'     > Category</label>
			<label  class='day'          >" . $row['day'      ] . " </label>
			<label  class='start'        >" . $row['start'    ] . " </label>
			<label  class='stop'         >" . $row['end'      ] . " </label>
			<label  class='convention'   >" . $row['name'     ] . "</label>
			<label  class='category'     >" . $row['category' ] . " </label>
			<label  class='location'     >" . $row['location' ] . " </label>
			<span   class='brief'        >" . $row['brief'    ] . " </span>
			<label  class='long'         >" . $row['extended' ] . " </label>
			<button class='expandButton' > Expand</button>
			<button class='addButton' " . ($row['event'] != null ? "hidden" : "") .   "> Add to schedule</button>
			<button class='removeButton'" . ($row['event'] == null ? "hidden" : "") . " > Remove from schedule</button>
            <label  class='conflictLabel' ". ($row['overlapping'] == 0 ? "hidden" : "") .">Time <br> conflict </label>
		</li>
		";
    }//end while

    echo "</ul>
    	</div>";
} else {
    echo "0 results";
}
$conn->close();
?>
</div>
<img src="../images/crowd.jpg" style="position:relative;left:2px;top:30px;">
</div>
</body>

</html>

<script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
<script src="../js/calendar.js"></script>