<html>

<head>
	<script src="http://code.jquery.com/jquery-3.2.0.js"></script>
	<link rel="stylesheet" href="../css/index.css">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="../css/dashboard.css">
</head>
<body>

<div class = "header">
Search</div>


<div class = "sidebar">

<ul>

<a href = "Dashboard.php">
<li>
Dashboard
</li>
</a>

<a href = "Calendar.php">
<li>
Calendar
</li>
</a>

<a href = "Schedule.php">
<li>
Schedule
</li>
</a>

<a href = "Search.php">
<li>
Search
</li>
</a>

</ul>

</div>

<div id="wonder">
<img src="../images/wonder.jpg">
</div>
<div id="thor">
<img src="../images/thor.jpg">
</div>
<div id="flash">
<img src="../images/flash.png">
</div>
<div id="hulk">
<img src="../images/hulk.jpg">
</div>

<div id="iron">
<img src="../images/iron.jpg">
</div>
<div id="superman">
<img src="../images/superman.png">
</div>

<div class = "filters">

		<div class = "searchBox">
		<input  type="text" class="searchTerm"> 
		<input  type="submit" class="Search" value="Search"> 					<!-- Div containing all search features-->
		</div>
	<!-- #######################################################################################-->

<ul class="drpdwn">
	<li class="drpdwn_Title"><p>Convention</p>
		<ul class="drpdwn_Content1">
		<input type="checkbox" value="Convention 1 " checked>Convention 1</input><br>
		<input type="checkbox" value="Convention 3" checked>Convention 3</input><br></ul>				<!-- Convention Filter-->
	</li>

	<!-- ############################################################################################-->

	<li class="drpdwn_Title"><p>Categories</p>
		<ul class="drpdwn_Content2">
		<input type="checkbox" value="games" checked>Games</input><br>					<!-- Category Filter-->
		<input type="checkbox" value="movies" checked>Movies</input><br></ul>
	</li>

	<!-- #############################################################################################-->

	<li class="drpdwn_Title"><p>Time</p>
		<ul class="drpdwn_Content3">
		<input type="checkbox" class="specificTime" unchecked>Search by Specific Time</input><br>
		<input type="time" class="time1" min="1" max="24" value="07:00" step="900"><br>			<!-- Time Filter-->
  		<input type="time" class="time2" min="1" max="24" value="22:00" step="900"><br></ul>
	</li>

	<!-- #################################################################################################-->

	<li class="drpdwn_Title"><p>Date</p>
		<ul class="drpdwn_Content4">
		<input type="checkbox" class="specificDate" unchecked>Search by Specific Date</input>
		<input type="date" value="2017-02-27">													<!-- Date Filter-->
		</ul>
	</li>

</ul>

</div>

<div id = "wrapper_Search">


<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "comic";

$conn = new mysqli($servername, $username, $password, $dbname);

if($conn->connect_error){
	die("connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM events INNER JOIN conventions on events.convention = conventions.id
							 LEFT JOIN schedules on events.eventid = schedules.event
							 ORDER BY events.day ASC;";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "
        <li class='event " . ($row['event'] != null ? "added" : "") . "'>
        	<input  class='eventId' type='text' value='" . $row['eventid'] . "' hidden>
	        <label  class='timeLabel'         > Time/Date</label>
			<label  class='descriptionLabel'  > Description</label>
			<label  class='conventionLabel'   > Convention</label>
			<label  class='locationLabel'     > Location</label>
			<label  class='categoryLabel'     > Category</label>
			<label  class='day'          >" . $row['day'      ] . " </label>
			<label  class='start'        >" . $row['start'    ] . " </label>
			<label  class='stop'         >" . $row['end'      ] . " </label>
			<label  class='convention'   >" . $row['name'     ] . " </label>
			<label  class='category'     >" . $row['category' ] . " </label>
			<label  class='location'     >" . $row['location' ] . " </label>
			<span   class='brief'        >" . $row['brief'    ] . " </span>
			<label  class='long'         >" . $row['extended' ] . " </label>
			<button class='expandButton' > Expand</button>
			<button class='addButton' " . ($row['event'] != null ? "hidden" : "") .   "> Add to schedule</button>
			<button class='removeButton'" . ($row['event'] == null ? "hidden" : "") . " > Remove from schedule</button>
		</li>
		";
 			}
} else {
    echo "0 results";
}
$conn->close();
?>
</ul>
</div>

<script>

$(".start").each(function(){

var t = String($(".start").text()).slice(0,5);
var y = String($(".stop").text()).slice(0,5);

$(".start").text(t);
$(".stop").text(y);
});


$(".Search").click(function(){	

$(".brief").parent().hide();
$(".brief:contains('"+ $(".searchTerm").val().toLowerCase() +"')").parent().show();			//Hides all events then diplays them
$(".long:contains('"+ $(".searchTerm").val().toLowerCase() +"')").parent().show();				//if the search term appears in
																							//brief or long discription
if($(".searchTerm").val().length==0){
	$(".brief").parent().show();
}
else{
	checkFilters(0);																					
}

});

//##########################################################################################################

$('.drpdwn_Content1 > input').change( function(){
//Calls everytime an input is changed with in this filter
	$('.drpdwn_Content1 >input').each(function(){
		//Cycles through every input in this filter
																							//If the value of the checkbox is equal to
		if($(this).is(":checked")==false){													//the name of the convention, and said checkbox
			$(".convention:contains('"+ $(this).val() +"')").parent().hide();				//is checked, then the event is displayed
		}	else{
			$(".convention:contains('"+ $(this).val() +"')").parent().show();
		}	
																					
	});
	checkFilters(1);
});

//##########################################################################################################

$('.drpdwn_Content2 > input').change( function(){

	$('.drpdwn_Content2 > input').each(function(){

	if($(this).is(":checked")==false){
			$(".category:contains('"+ $(this).val() +"')").parent().hide();
		}	else{
			$(".category:contains('"+ $(this).val() +"')").parent().show();
		}	

			
	});		
	checkFilters(2);																					//Hides event if category matches
});

//##########################################################################################################

$('.drpdwn_Content3 > input').change( function(){

	if($(".specificTime").is(":checked")==true && $(".searchTerm").val().length==0){

	$('.drpdwn_Content3 > input').each(function(){

		$('.start').each(function(){

		 u = $(this).text();
	
		u = String(u).split(":");
		
		var user = u[0]+u[1];

		var srt = $(".time1").val().split(":");
		var stp = $(".time2").val().split(":");

		var beg = srt[0]+srt[1];
		var fin = stp[0]+stp[1];

		if(parseInt(beg)<99||parseInt(beg)==1200)
			beg=parseInt(beg)+1200;																//Since we are dealing with a time range
																								//I had to find the numerical value
		if($(".time1").val().indexOf("PM")>=0)													//of the times then compared them and
			beg=parseInt(beg)+1200;																//hid the events that fell in the time range


		if(parseInt(fin)<99||parseInt(fin)==1200)
			fin=parseInt(fin)+1200;

		if($(".time2").val().indexOf("PM")>=0)
			fin=parseInt(fin)+1200;

			if(parseInt(user)<parseInt(beg) || parseInt(user)>parseInt(fin)){
				$(this).parent().hide();
			}	else{
				$(this).parent().show();
			}		

		})
	});	

	}	
	if($(".specificTime").is(":checked")==false && $(".searchTerm").val().length==0)
		$(".start").parent().show();

	checkFilters(3);
																					
});

//##########################################################################################################

$('.drpdwn_Content4 > input').change( function(){

if($(".specificDate").is(":checked")==true && $(".searchTerm").val().length==0){

	$('.drpdwn_Content4 > input').each(function(){
				
		if($(".day").is(":contains('"+$(this).val()+"')")==false){
			$(".day").parent().hide();
		}	else{
			$(".day:contains('"+$(this).val()+"')").parent().show();
		}

	});		

}
if($(".specificDate").is(":checked")==false && $(".searchTerm").val().length==0)
		$(".day").parent().show();

	checkFilters(4);

});

//##########################################################################################################

function checkFilters(x){

if(x!=1){
	$('.drpdwn_Content1 >input').each(function(){
																					
		if($(this).is(":checked")==false)												
			$(".convention:contains('"+ $(this).val() +"')").parent().hide();				
																		
	});
}
if(x!=2){
	$('.drpdwn_Content2 > input').each(function(){

	if($(this).is(":checked")==false)
			$(".category:contains('"+ $(this).val() +"')").parent().hide();
	
	});			
}

if(x!=3){
	if($(".specificTime").is(":checked")==true){

		$('.drpdwn_Content3 > input').each(function(){

			$('.start').each(function(){

			var u = $(".start").text();
			u = String(u).split(":");

			var user = u[0]+u[1];

			var srt = $(".time1").val().split(":");
			var stp = $(".time2").val().split(":");

			var beg = srt[0]+srt[1];
			var fin = stp[0]+stp[1];

			if(parseInt(beg)<99||parseInt(beg)==1200)
				beg=parseInt(beg)+1200;

			if($(".time1").val().indexOf("PM")>=0)
				beg=parseInt(beg)+1200;


			if(parseInt(fin)<99||parseInt(fin)==1200)
				fin=parseInt(fin)+1200;

			if($(".time2").val().indexOf("PM")>=0)
				fin=parseInt(fin)+1200;


				if(parseInt(user)<parseInt(beg) || parseInt(user)>parseInt(fin)){
				$(this).parent().hide();
			}	

				});
		});	
	}	
}

if(x!=4){

	if($(".specificDate").is(":checked")==true){

			$('.drpdwn_Content4 > input').each(function(){


				if($(".day").is(":contains('"+$(this).val()+"')")==false)
					$(".day").parent().hide();

			
			});		
	}														
}

if($(".searchTerm").val().length!=0){
if(x!=0){

if($(".brief").is(":contains('"+ $(".searchTerm").val().toLowerCase() +"')")==false){
			$(".brief").parent().hide();														
				}
		}
	}
		
}
</script>

</body>
</html>