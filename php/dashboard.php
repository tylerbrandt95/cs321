<html>

<head>
	<link rel="stylesheet" href="../css/index.css">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="../css/dashboard.css">
</head>
<body>

<div class = "header">
Dashboard</div>


<div class = "sidebar">

<ul>

<a href = "Dashboard.php">
<li>
Dashboard
</li>
</a>

<a href = "Calendar.php">
<li>
Calendar
</li>
</a>

<a href = "Schedule.php">
<li>
Schedule
</li>
</a>

<a href = "Search.php">
<li>
Search
</li>
</a>

</ul>

</div>

<div id="wonder">
<img src="../images/wonder.jpg">
</div>
<div id="thor">
<img src="../images/thor.jpg">
</div>
<div id="flash">
<img src="../images/flash.png">
</div>
<div id="hulk">
<img src="../images/hulk.jpg">
</div>

<div id="iron">
<img src="../images/iron.jpg">
</div>
<div id="superman">
<img src="../images/superman.png">
</div>



<!-- Written by Tyler Brandt -->

<div id="addNotification">Event Added!</div>
<div id="removeNotification">Event Removed :(</div>
<div id="overlapNotification">
    Event Added! <br>This event has caused a time conflict in your schedule
</div>

<div id="wrapper">

	<?php //sets up the database connection
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "comic";

	$conn = new mysqli($servername, $username, $password, $dbname);

	if($conn->connect_error){
		die("connection failed: " . $conn->connect_error);
	}
	?>
	<?php //Begin featured events
	
	$sql = "SELECT * FROM events INNER JOIN conventions on events.convention = conventions.id 
								 LEFT JOIN schedules on events.eventid = schedules.event
								 WHERE featured = true 
								 ORDER BY events.day ASC;";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		echo "<div id='featured'>";
		echo "<div id='featuredHeader'><h2>Featured Events</h2></div>";
		echo "<div id='featuredevents' hidden>";
	    while($row = $result->fetch_assoc()) {
	        echo "
        <li class='event " . ($row['event'] != null ? "added" : "") . "'>
        	<input  class='eventId' type='text' value='" . $row['eventid'] . "' hidden>
	        <label  class='timeLabel'         > Time/Date</label>
			<label  class='descriptionLabel'  > Description</label>
			<label  class='conventionLabel'   > Convention</label>
			<label  class='locationLabel'     > Location</label>
			<label  class='categoryLabel'     > Category</label>
			<label  class='day'          >" . $row['day'      ] . " </label>
			<label  class='start'        >" . $row['start'    ] . " </label>
			<label  class='stop'         >" . $row['end'      ] . " </label>
			<label  class='convention'   >" . $row['name'     ] . " </label>
			<label  class='category'     >" . $row['category' ] . " </label>
			<label  class='location'     >" . $row['location' ] . " </label>
			<span   class='brief'        >" . $row['brief'    ] . " </span>
			<label  class='long'         >" . $row['extended' ] . " </label>
			<button class='expandButton ui-widget' > Expand</button>
			<button class='addButton ui-widget ' " . ($row['event'] != null ? "hidden" : "") .   "> Add to schedule</button>
			<button class='removeButton ui-widget'" . ($row['event'] == null ? "hidden" : "") . " > Remove from schedule</button>
			<label  class='conflictLabel' ". ($row['overlapping'] == 0 ? "hidden" : "") .">Time <br> conflict </label>
		</li>
		";
	    }//end while
	    echo "</div>";
	    echo "</div>";
	} else {
	    echo "0 results";
	}
	//End featured events

	?>
	<br><br>

	<?php //Begin schedule at a glance
	
	$sql = "SELECT * FROM events INNER JOIN conventions on events.convention = conventions.id
								 INNER JOIN schedules on events.eventid = schedules.event
								 ORDER BY events.day ASC;";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		echo "<div id='schedule'>";
		echo "<div id='scheduleHeader'><h2>My Schedule at a glance</h2></div>";
		echo "<div id='scheduleEvents' hidden>";
	    while($row = $result->fetch_assoc()) {
	        echo "
	        <li class='event added'>
	        	<input  class='eventId' type='text' value='" . $row['eventid'] . "' hidden>
		        <label  class='timeLabel'         > Time/Date</label>
				<label  class='descriptionLabel'  > Description</label>
				<label  class='conventionLabel'   > Convention</label>
				<label  class='locationLabel'     > Location</label>
				<label  class='categoryLabel'     > Category</label>
				<label  class='day'          >" . $row['day'      ] . " </label>
				<label  class='start'        >" . $row['start'    ] . " </label>
				<label  class='stop'         >" . $row['end'      ] . " </label>
				<label  class='convention'   >" . $row['name'     ] . " </label>
				<label  class='category'     >" . $row['category' ] . " </label>
				<label  class='location'     >" . $row['location' ] . " </label>
				<span   class='brief'        >" . $row['brief'    ] . " </span>
				<label  class='long'         >" . $row['extended' ] . " </label>
				<button class='expandButton ui-widget' > Expand</button>
				<button class='removeButton ui-widget' > Remove from schedule</button>
				<label  class='conflictLabel' ". ($row['overlapping'] == 0 ? "hidden" : "") .">Time <br> conflict </label>
			</li>
			";
	    }//end while
	    echo "</div>";
	    echo "</div>";
	} else {
	    echo "0 results";
	}
	//End schedule at a glance

	$conn->close();
	?>
	<img src="../images/crowd.jpg" style="position:relative;left:2px;top:30px;">
</div>

</body>
</html>

<script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
<script src="../js/dashboard.js"></script>