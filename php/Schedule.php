<html>

<head>
<meta charset = "UTF-8">
	<link rel="stylesheet" href="../css/index.css">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="../css/schedule.css">
	
</head>
<body>

<div class = "header">
Schedule</div>


<div class = "sidebar">

<ul>

<a href = "dashboard.php">
<li>
Dashboard
</li>
</a>

<a href = "calendar.php">
<li>
Calendar
</li>
</a>

<a href = "schedule.php">
<li>
Schedule
</li>
</a>

<a href = "search.php">
<li>
Search
</li>
</a>

</ul>

</div>

<div id="wonder">
<img src="../images/wonder.jpg">
</div>
<div id="thor">
<img src="../images/thor.jpg">
</div>
<div id="flash">
<img src="../images/Flash.png">
</div>
<div id="hulk">
<img src="../images/hulk.jpg">
</div>

<div id="iron">
<img src="../images/iron.jpg">
</div>
<div id="superman">
<img src="../images/superman.png">
</div>


<div class = "main">

<ul class="eventList">
	<li class="event">
		<label  class="timeLabel"         > Time/Date</label>
		<label  class="descriptionLabel"  > Description</label>
		<label  class="conventionLabel"   > Convention</label>
		<label  class="locationLabel"     > Location</label>
		<label  class="categoryLabel"     > Category</label>

		<span class = "contentText">
		<label  class="day"          > 02/02/2017</label>
		<label  class="start"        > 10:00 AM</label>
		<label  class="stop"         > 11:00 AM</label>
		<span   class="brief"        > This is a brief description</span>
		<label  class="long"         > This is a long description</label>
		<label  class="convention"   > Comic Con West</label>
		<label  class="location"     > Room 5</label>
		<label  class="category"     > Video Games</label>
		<button class="expandButton" > Expand</button>
		<button class="addButton"    > Add to schedule</button>
		<button class="removeButton" > Remove from schedule</button>
		<span>
	</li>
	<li class="event">
		<label  class="timeLabel"         > Time/Date</label>
		<label  class="descriptionLabel"  > Description</label>
		<label  class="conventionLabel"   > Convention</label>
		<label  class="locationLabel"     > Location</label>
		<label  class="categoryLabel"     > Category</label>

		<span class = "contentText">
		<label  class="day"          > 02/02/2017</label>
		<label  class="start"        > 10:00 AM</label>
		<label  class="stop"         > 11:00 AM</label>
		<label  class="convention"   > Comic Con West</label>
		<label  class="category"     > Video Games</label>
		<label  class="location"     > Room 5</label>
		<span   class="brief"        > This is a brief description</span>
		<label  class="long"         > This is a long description</label>
		<button class="expandButton" > Expand</button>
		<button class="addButton"    > Add to schedule</button>
		<button class="removeButton" > Remove from schedule</button>
		</span>
	</li>
</ul>

</div>
<script src="https://code.jquery.com/jquery-3.2.0.js" ></script>

<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="../js/schedule.js"></script>
</body>
</html>