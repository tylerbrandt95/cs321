<?php

$id = $_POST['id'];

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "comic";

$conn = new mysqli($servername, $username, $password, $dbname);

if($conn->connect_error){
	die("connection failed: " . $conn->connect_error);
}

//check to see if the event is already in the schedule
$sql = "SELECT * FROM schedules INNER JOIN events on schedules.event = events.eventid;";
$result = $conn->query($sql);

$overlapping = '0';
while($row = $result->fetch_assoc()){
	//check to see if there are any overlapping events
	$sql = "SELECT * FROM schedules INNER JOIN events on schedules.event = events.eventid WHERE events.day = '". $row['day'] ."';";
	$result = $conn->query($sql);

	while($event = $result->fetch_assoc()){

		if($row['start'] >= $event['start'] && $row['start'] <= $event['end'] || $row['end'] >= $event['start'] && $row['end'] <= $event['end'] ){
			$overlapping = '1';
		}
	}
}


$sql = "INSERT INTO schedules (user, event, overlapping) VALUES (1, " . $id . ", ". $overlapping .");";
$result = $conn->query($sql);

if($result == true){
	if($overlapping == '0'){
		echo "success";
	}else{
		echo "overlap";
	}
}else{
	echo "failure";
}

?>