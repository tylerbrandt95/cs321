$(function(){
	$('.expandButton').button();
	$('.removeButton').button();
	$('.addButton').button();

	$('.expandButton').on("click", initializeExpandButton);

	$('.addButton').on("click", function(){
		var time = $(this).siblings('.time').html();
		//Extract the date, start, and stop strings from the time element
		var date = time.substring(time.indexOf('<u>') + 3, time.indexOf('</u>'));
		var start = time.substring(time.indexOf('Start: ') + 7, time.indexOf('<br>Stop'));
		var stop = time.substring(time.indexOf('Stop: ') + 6, time.length);

		var brief = $(this).siblings('.brief').html();
		var long  = $(this).siblings('.long').html();
		addToSchedule(date, start, stop, brief,long);
		addEvent($(this));
	});


});

function addEvent(self){
	self.parent().addClass("added");
	self.html("Remove from schedule");
	self.removeClass("addButton");
	self.addClass("removeButton");
	self.unbind("click");
	// self.on("click", removeEvent);
	self.parent().prop("title", "This event has been added to your schedule");
	self.parent().tooltip({position:{at:"center top", my:"center bottom"}});
}

function addToSchedule(date, start, stop, brief, long){

	var event = `
				<li class="event">
					<label class="time"><u>${date}</u><br>Start: ${start}<br>Stop: ${stop}</label>
					<span class="brief">${brief}</span>
					<label class="long">${long}</label>
					<button class="expandButton newButton">Expand</button>
					<button class="removeButton newButton">Remove from schedule</button>
				</li>
				`;

	$('#scheduleList').append(event);
	$('.newButton').button();
	$('.newButton').unbind("click");
	$('.newButton').on("click", initializeExpandButton);
	$('.newButton').parent().css("background-color", "#B24926");
	$('.newButton').parent().animate({ backgroundColor: "#FFFFFF"}, {complete: function() { $('.event').attr("style", "");}});
	$('.newButton').removeClass("newButton");
	$('#addNotification').css("visibility", "visible");
	$('#addNotification').animate({backgroundColor: "#FFFFFF"}, {complete: function() { $('#addNotification').css("visibility", "hidden"); $('#addNotification').attr("style", "");}, duration: 1000});
}

function initializeExpandButton(){
	if($(this).prop("expanded") == "true"){
		$(this).parent().css("height", "80px");
		$(this).siblings('.long').css("visibility", "hidden");
		$(this).prop("expanded", "false");
	}else{
		$(this).parent().css("height", "150px");
		$(this).siblings('.long').css("visibility", "visible");
		$(this).prop("expanded", "true");
	}
}
