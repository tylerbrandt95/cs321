$(function(){
	$('#accordion').accordion();
	$('.expandButton').button();
	$('.removeButton').button();
	$('.addButton').button();
	$('.added').tooltip({position:{at:"center top", my:"center bottom"}});

	$('.expandButton').on("click", initializeExpandButton);
	$('.addButton').on("click", addEvent);
	$('.removeButton').on("click", removeEvent);
});

function addEvent(){
	$(this).parent().addClass("added");
	$(this).html("Remove from schedule");
	$(this).removeClass("addButton");
	$(this).addClass("removeButton");
	$(this).unbind("click");
	$(this).on("click", removeEvent);
	$(this).parent().prop("title", "This event has been added to your schedule");
	$(this).parent().tooltip({position:{at:"center top", my:"center bottom"}});
	$(this).parent().tooltip('enable');
	$('#addNotification').css("visibility", "visible");
	$('#addNotification').animate({backgroundColor: "#FFFFFF"}, {complete: function() { $('#addNotification').css("visibility", "hidden"); $('#addNotification').attr("style", "");}, duration: 1000});
}
function removeEvent(){
	$(this).parent().removeClass("added");
	$(this).html("Add to schedule");
	$(this).removeClass("removeButton");
	$(this).addClass("addButton");
	$(this).on("click", addEvent);
	$(this).parent().prop("title", "");
	$(this).parent().tooltip('disable');
	$('#removeNotification').css("visibility", "visible");
	$('#removeNotification').animate({backgroundColor: "#FFFFFF"}, {complete: function() { $('#removeNotification').css("visibility", "hidden"); $('#removeNotification').attr("style", "");}, duration: 1000});
}

function initializeExpandButton(){
	if($(this).prop("expanded") == "true"){
		$(this).parent().css("height", "80px");
		$(this).siblings('.long').css("visibility", "hidden");
		$(this).prop("expanded", "false");
	}else{
		$(this).parent().css("height", "150px");
		$(this).siblings('.long').css("visibility", "visible");
		$(this).prop("expanded", "true");
	}
}